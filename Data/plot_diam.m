clc
clear all

load odom_diam_15_min.mat
load lidar_diam_15_min.mat

data_odom = table2array(odomdiam15min);
data_lidar = table2array(lidardiam15min);

odom_z = abs(data_odom(:,4));
lidar_z = abs(data_lidar(:,4) - data_lidar(1,4));

figure(1)
plot(odom_z)

figure(2)
plot(lidar_z)