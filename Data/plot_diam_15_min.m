clc
clear all

addpath('rosbag_convert_data')

load 'data_diam.mat'

data_diam_array = table2array(DataDiam);

x = linspace(1, 154, 154);
gyro = data_diam_array(:,1);
licp = data_diam_array(:,2);
ekf  = data_diam_array(:,3);

plot(x, gyro, x, licp, x, ekf)