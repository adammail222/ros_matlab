clc
clear all

load odom_coba.mat
load lidar_coba.mat
load hasil_ekf_coba_degree.mat

data_odom = table2array(odomcoba);
data_lidar = table2array(lidarcoba);
data_ekf = table2array(hasilekfcobadegree);

odom_x = data_odom(:,2);
odom_y = data_odom(:,3);
odom_z = data_odom(:,4);

lidar_x = data_lidar(:,2);
lidar_y = data_lidar(:,3);
lidar_z = data_lidar(:,4);

