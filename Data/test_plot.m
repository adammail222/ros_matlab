clc
clear all

addpath('rosbag_convert_data')

% load data_rr_3.mat
% load monitor_4.mat

% 
% load sensor_manual_1.mat
% load lidar_manual_1.mat
% load ekf_manual_1.mat
% 
% load sensor_auto_0_5.mat
% load lidar_auto_0_5.mat
% load ekf_auto_0_5.mat
% 
load sensor_auto_1.mat
load lidar_auto_1.mat
load ekf_auto_1.mat

% load sensor_auto_1_5.mat
% load lidar_auto_1_5.mat
% load ekf_auto_1_5.mat
% 
load sensor_auto_2.mat
load lidar_auto_2.mat
load ekf_auto_2.mat
% 
load sensor_auto_2_5.mat
load lidar_auto_2_5.mat
load ekf_auto_2_5.mat
% 
% load sensor_diam.mat
% load lidar_diam.mat
% load ekf_diam.mat
% 
% load sensor_manual_new.mat
% load lidar_manual_new.mat
% load ekf_manual_new.mat

load sensor_calib.mat
load lidar_calib.mat
load ekf_calib.mat

% data_odom = table2array(sensormanual1);
% data_lidar = table2array(lidarmanual1);
% data_ekf = table2array(ekfmanual1);

% data_odom = table2array(sensorauto05);
% data_lidar = table2array(lidarauto05);
% data_ekf = table2array(ekfauto05);
% 
% data_odom = table2array(sensorauto1);
% data_lidar = table2array(lidarauto1);
% data_ekf = table2array(ekfauto1);

% data_odom = table2array(sensorauto15);
% data_lidar = table2array(lidarauto15);
% data_ekf = table2array(ekfauto15);

% data_odom = table2array(sensorauto2);
% data_lidar = table2array(lidarauto2);
% data_ekf = table2array(ekfauto2);

data_odom = table2array(sensorauto25);
data_lidar = table2array(lidarauto25);
data_ekf = table2array(ekfauto25);


% data_odom = table2array(sensorcalib);
% data_lidar = table2array(lidarcalib);
% data_ekf = table2array(ekfcalib);

% data_odom = table2array(monitor4);


% data_odom = table2array(sensordiam);
% data_lidar = table2array(lidardiam);
% data_ekf = table2array(ekfdiam);

% data_odom = table2array(sensormanualnew);
% data_lidar = table2array(lidarmanualnew);
% data_ekf = table2array(ekfmanualnew);

odom_x = data_odom(:,2);
odom_y = data_odom(:,3);
odom_z = data_odom(:,4);

lidar_x = data_lidar(:,2) - data_lidar(1,2);
lidar_y = data_lidar(:,3) - data_lidar(1,3);
lidar_z = data_lidar(:,4) - data_lidar(1,4);

n_samp = floor(length(odom_x)/length(lidar_x))

odom_x_new = odom_x(1:n_samp:end,:);
odom_y_new = odom_y(1:n_samp:end,:);
odom_z_new = odom_z(1:n_samp:end,:);

ekf_x = data_ekf(:,2) - data_ekf(1,2);
ekf_y = data_ekf(:,3) - data_ekf(1,3);
ekf_z = data_ekf(:,4) - data_ekf(1,4);

odom_z_rad = (odom_z-90) * (pi/180);
lidar_z_rad = (lidar_z-90) * (pi/180);
ekf_z_rad = (ekf_z-90) * (pi/180);

% Define the robot's position and orientation
robotPosOdom = [odom_x, odom_y];   % Robot position [x, y]
robotAngleOdom = odom_z_rad;   % Robot orientation in radians

robotPosLidar = [lidar_x, lidar_y];   % Robot position [x, y]
robotAngleLidar = lidar_z_rad;   % Robot orientation in radians

robotPosEkf = [ekf_x, ekf_y];   % Robot position [x, y]
robotAngleEkf = ekf_z_rad;   % Robot orientation in radians

% Define the robot's dimensions
robotLength = 0.1;     % Length of the robot arrow
robotWidth = 0.1;    % Width of the robot arrow

% Compute the end point of the robot arrow
dx_Odom = robotLength * cos(robotAngleOdom);
dy_Odom = robotLength * sin(robotAngleOdom);
endPoint = robotPosOdom + [dx_Odom, dy_Odom];

dx_Lidar = robotLength * cos(robotAngleLidar);
dy_Lidar = robotLength * sin(robotAngleLidar);
endPoint = robotPosLidar + [dx_Lidar, dy_Lidar];

dx_Ekf = robotLength * cos(robotAngleEkf);
dy_Ekf = robotLength * sin(robotAngleEkf);
endPoint = robotPosEkf + [dx_Ekf, dy_Ekf];

% Plot the robot
figure;
hold on;
axis equal;

% Plot the robot position
% plot(robotPos(:,1), robotPos(:,2), 'ro', 'MarkerSize', 10);

% Plot Lapangan
x_lapangan = [-3   2.9  2.9]
y_lapangan = [1.95 1.95 7 ]

x_lapangan_2 = [-3   3.5  3.5]
y_lapangan_2 = [1.35 1.35 7 ]

x_lapangan_3 = [-2 -2 1 1]
y_lapangan_3 = [7.475 4.475 4.475 7.475]

x_pole=[2.7 0.8 0]
y_pole=[2.15 4.675 2.15]

x_titik_auto = [0 1.77 2.519 4.519 4.442 4.317 4.4]
y_titik_auto = [0 0.263 0.318 0.518 2.202 4.586 5]

% Plot the robot arrow
plot_rg = plot(odom_x_new, odom_y_new, '-','Color', 'b', 'DisplayName', 'RG');
plot_lidar = plot(lidar_x, lidar_y, '-','Color', 'r', 'DisplayName', 'ICP');
plot_ekf = plot(ekf_x, ekf_y, '-','Color', 'k', 'DisplayName', 'EKF');
plot_titik = plot(x_titik_auto, y_titik_auto, 'x', 'DisplayName', 'Waypoint');
%    query_data = data_odom(data_odom(:,7) > 0,:);
%    X=query_data(:,14)
%    Y=query_data(:,15)
   
%    plot(X,Y,'x')
% query_data_lidar = data_lidar((data_lidar(:,2) < 5 & data_lidar(:,3) < 5),:);
% query_data_ekf = data_ekf((data_ekf(:,2) < 5 & data_ekf(:,3) < 5),:);

% plot_lidar = plot(query_data_lidar(:,2), query_data_lidar(:,3), '-','Color', 'r', 'DisplayName', 'ICP');
% plot_ekf = plot(query_data_ekf(:,2), query_data_ekf(:,3), '-','Color', 'k', 'DisplayName', 'ICP');

plot(x_pole, y_pole, 'O')
plot(x_lapangan, y_lapangan, x_lapangan_2, y_lapangan_2, x_lapangan_3, y_lapangan_3,'Color', 'g', 'LineWidth', 1)

hold off
legend([plot_rg plot_lidar plot_ekf plot_titik], {'RG', 'ICP', 'EKF', 'Waypoint'},'Location','northwest')
% legend([plot_rg], {'RG'},'Location','northwest')
% legend([plot_lidar], {'ICP'},'Location','northwest')
% legend([plot_ekf], {'EKF'},'Location','northwest')



% quiver(robotPosOdom(:,1), robotPosOdom(:,2), dx_Odom, dy_Odom, 0, 'b', 'LineWidth', 1, 'MaxHeadSize', 0.5);
% quiver(robotPosLidar(:,1), robotPosLidar(:,2), dx_Lidar, dy_Lidar, 0, 'r', 'LineWidth', 1, 'MaxHeadSize', 0.5);
% quiver(robotPosEkf(:,1), robotPosEkf(:,2), dx_Ekf, dy_Ekf, 0, 'k' , 'LineWidth', 1, 'MaxHeadSize', 0.5);

% Plot the robot arrow's body
bodyWidthOdom = robotWidth / 2;
bodyEndOdom = robotPosOdom + [dx_Odom * 0.8, dy_Odom * 0.8];
bodyStartOdom = robotPosOdom - [dx_Odom * 0.4, dy_Odom * 0.4];
bodyPerpendicularOdom = [dy_Odom, -dx_Odom] * bodyWidthOdom;
bodyPointsOdom = [bodyEndOdom + bodyPerpendicularOdom; bodyEndOdom - bodyPerpendicularOdom; bodyStartOdom - bodyPerpendicularOdom; bodyStartOdom + bodyPerpendicularOdom];
% fill(bodyPointsOdom(:,1), bodyPointsOdom(:,2), 'b');

bodyWidthLidar = robotWidth / 2;
bodyEndLidar = robotPosLidar + [dx_Lidar * 0.8, dy_Lidar * 0.8];
bodyStartLidar = robotPosLidar - [dx_Lidar * 0.4, dy_Lidar * 0.4];
bodyPerpendicularLidar = [dy_Lidar, -dx_Lidar] * bodyWidthLidar;
bodyPointsLidar = [bodyEndLidar + bodyPerpendicularLidar; bodyEndLidar - bodyPerpendicularLidar; bodyStartLidar - bodyPerpendicularLidar; bodyStartLidar + bodyPerpendicularLidar];
% fill(bodyPointsLidar(:,1), bodyPointsLidar(:,2), 'r');

bodyWidthEkf = robotWidth / 2;
bodyEndEkf = robotPosEkf + [dx_Ekf * 0.8, dy_Ekf * 0.8];
bodyStartEkf = robotPosEkf - [dx_Ekf * 0.4, dy_Ekf * 0.4];
bodyPerpendicularEkf = [dy_Ekf, -dx_Ekf] * bodyWidthEkf;
bodyPointsEkf = [bodyEndEkf + bodyPerpendicularEkf; bodyEndEkf - bodyPerpendicularEkf; bodyStartEkf - bodyPerpendicularEkf; bodyStartEkf + bodyPerpendicularEkf];
% fill(bodyPointsEkf(:,1), bodyPointsEkf(:,2), 'k');

% for i = 1:length(data_odom)

   
    
% end
% Set the plot limits
xlim([-0.5, 6]);
ylim([-0.5, 6]);

% Add labels
title('Posisi dan Orientasi Robot Gerak Otomatis (v = 0.5 m/s)');
xlabel('X-axis (m)');
ylabel('Y-axis (m)');
str={'Lapangan','ABU ROBOCON 2023'};
text(0,3, str, 'Color', 'g');

% legend({'RG', 'ICP', 'EKF'},'Location','northwest')
% legend({'RG'},'Location','northwest')
% legend({'ICP'},'Location','northwest')
% legend({'EKF'},'Location','northwest')

% Show the plot
% hold off;