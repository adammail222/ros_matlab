# ros_matlab

Hello.....

This script is using for practicing the ROS with MATLAB.
Specially if you are in the final semester and want to learn MATLAB and ROS.

This can guide you to practice MATLAB with simple function and command.
Bassicaly the final project (Skripsi) is need to make plot or diagram.
If you are using ROS in Linux Ubuntu and want to make plot or diagram in MATLAB environment.
You can use rosbag or save that data in CSV format.

this is the code for save the file in CSV file format.

```python
rostopic echo -b <bagfile>.bag -p <topic_name> > output.csv
```

if you are want to discuss, you can message me

adammail222@gmail.com