%NOTED
%this script using for create custom message ros msgs with MATLAB
%this technic beneficial for debugging the data
%Created by Adam Mail e59

%clear all workspace
clear all

%shutdown ros before activated
rosshutdown

%rosinit for activated the master
rosinit

%spesify the local port for node
masterhost = 'localhost';

%spesify the type of message
blankMsg1 = rosmessage("std_msgs/String","DataFormat","struct")
blankMsg2 = rosmessage("std_msgs/Int16","DataFormat","struct")
blankMsg3 = rosmessage("sensor_msgs/LaserScan","DataFormat","struct")

msgArray = [blankMsg1 blankMsg1 blankMsg2 blankMsg3]

msgArray(1).Data = "Hello, this is string :)"
msgArray(2).Data = "I hope your day is beautiful :)"
msgArray(3).Data = 1

allData = {msgArray.Data}

%if you want to make custom message from package:
%https://www.mathworks.com/help/ros/ug/create-custom-messages-from-ros-package.html