% Noted
% this script using for activated the master and connect node
% can help you to navigated the disbtributed of data because powerfull of
% MATLAB. Hope this can help :)
% created by Adam Mail e59
% URI = Uniform Resource Identifier

%clear all workspace
clear all

%shutdown ros before activated
rosshutdown

%rosinit for activated the master
rosinit

%spesify the local port for node
masterhost = 'localhost';

%create node with localhost
%syntax -> ros.Node('node_name', localhost)
%this syntax will be different if you want to differ the host, depend with
%your necessary.
node_1 = ros.Node('/test_node_1', masterhost) % node 1 with name test_node_1
node_2 = ros.Node('/test_node_2', masterhost) % node 2 with name test_node_2

%define the publisher and subscriber
%syntax publisher -> ros.Pubsliher(class_node_name, 'topic', 'type_of_msgs');
%syntax subscriber -> ros.Subscriber(class_node_name, 'topic', 'type_of_msgs');
pub = ros.Publisher(node_1, '/chatter', 'std_msgs/String');
sub = ros.Subscriber(node_2, '/chatter', 'std_msgs/String');

%define the type of message
%syntax for define msgs -> rosmessage('type_of_message')
%syntax for input the data -> msg.Data = the_data;
%the data it depends of your type of message, example for basic message:
%type of message -> msg = sensor_msgs/LaserScan , so if you want to see the data:
%msg.Header or msg.AngleMin or msg.AngleMax, etc...
msg = rosmessage('std_msgs/String');
msg.Data = 'Message from Node 1';

send(pub,msg)
pause(1)
sub.LatestMessage
msg.showdetails