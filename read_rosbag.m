%NOTED
%this script is using for read data from rosbag. This is useful if you want
%to analyze the data of the robot in MATLAB :)
%Created by Adam Mail e59

%clear all workspace
clear all

%shutdown ros before activated
rosshutdown

%rosinit for activated the master
rosinit

%spesify the local port for node
masterhost = 'localhost';

%select the rosbag
bagselect = rosbag('example_from_tugas_akhir.bag');

%select the topic that want to read
bSel1 = select(bagselect,'Topic','/geometry_msgs/Point32');

msgStructs = readMessages(bagselect,'DataFormat','struct');
msgArray = cell2mat(msgStructs);

numElements = numel(msgArray); % Get the number of elements in the struct array
fieldValues = zeros(numElements, 3); % Initialize an array to store the field values

%fill the element using array for easy way to plot
for i = 1:numElements
    fieldValues(i,1) = msgArray(i).X;
    fieldValues(i,2) = msgArray(i).Y;
    fieldValues(i,3) = msgArray(i).Z;
end

%plot the graph
figure;
hold on;
axis equal;

x_lapangan = [-3   2.9  2.9]
y_lapangan = [1.95 1.95 7 ]

x_lapangan_2 = [-3   3.5  3.5]
y_lapangan_2 = [1.35 1.35 7 ]

x_lapangan_3 = [-2 -2 1 1]
y_lapangan_3 = [7.475 4.475 4.475 7.475]

x_pole=[2.7 0.8 0]
y_pole=[2.15 4.675 2.15]

x_titik_auto = [0 1.77 2.519 4.519 4.442 4.317 4.4]
y_titik_auto = [0 0.263 0.318 0.518 2.202 4.586 5]

plot_rg = plot(fieldValues(bagselect.AvailableTopics.NumMessages(2):bagselect.AvailableTopics.NumMessages(3),1), fieldValues(bagselect.AvailableTopics.NumMessages(2):bagselect.AvailableTopics.NumMessages(3),2), '-','Color', 'b', 'DisplayName', 'RG');
plot_lidar = plot(fieldValues(bagselect.AvailableTopics.NumMessages(1):bagselect.AvailableTopics.NumMessages(2),1), fieldValues(bagselect.AvailableTopics.NumMessages(1):bagselect.AvailableTopics.NumMessages(2),2), '-','Color', 'r', 'DisplayName', 'ICP');
plot_ekf = plot(fieldValues(1:bagselect.AvailableTopics.NumMessages(1),1), fieldValues(1:bagselect.AvailableTopics.NumMessages(1),2), '-','Color', 'k', 'DisplayName', 'EKF');
plot_titik = plot(x_titik_auto, y_titik_auto, 'x', 'DisplayName', 'Waypoint');

plot(x_pole, y_pole, 'O')
plot(x_lapangan, y_lapangan, x_lapangan_2, y_lapangan_2, x_lapangan_3, y_lapangan_3,'Color', 'g', 'LineWidth', 1)

hold off
legend([plot_rg plot_lidar plot_ekf plot_titik], {'RG', 'ICP', 'EKF', 'Waypoint'},'Location','northwest')

% Set the plot limits
xlim([-0.5, 6]);
ylim([-0.5, 6]);

% Add labels
title('Posisi dan Orientasi Robot Gerak Otomatis (v = 1.0 m/s)');
xlabel('X-axis (m)');
ylabel('Y-axis (m)');
str={'Lapangan','ABU ROBOCON 2023'};
text(0,3, str, 'Color', 'g');
